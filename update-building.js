const mongoose = require('mongoose')
const Building = require('./models/Building')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
// const Building = require('./models/Building')

async function main () {
  const newinformaticsBuilding = await Building.findById('621934c0ea15193d5fc95b85')
  const room = await Room.findById('621934c0ea15193d5fc95b86')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newinformaticsBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newinformaticsBuilding
  newinformaticsBuilding.rooms.push(room)
  informaticsBuilding.rooms.pull(room)
  room.save()
  newinformaticsBuilding.save()
  informaticsBuilding.save()
}
main().then(() => {
  console.log('Finish')
})
