const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/example')
const Room = require('./models/Room')
const Building = require('./models/Building')

async function main () {
  // update
  // const room = await Room.findById('621934c0ea15193d5fc95b86')
  // room.capacity = 20
  // room.save()
  // console.log(room)
  // findOne
  const room = await Room.findOne({ _id: '621934c0ea15193d5fc95b88' }).populate('building')
  console.log(room)
  console.log('---------------------------------------------')
  // find
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  const building = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(building))
}
main().then(() => {
  console.log('Finish')
})
